![Logo](https://gitlab.com/godot-groups/godot-git-control/raw/master/godot_git_control_logo.png)

# Godot Git Control #

This is a simple Git Integration plugin for @godotengine.

![Screenshot](https://gitlab.com/godot-groups/godot-git-control/raw/master/Screenshot_20180305_172510.png)

Currently this plugin is in alpha, but is usable, if you encounter any bugs, please let me know.

This plugin plugin should work on Linux (tested), Mac and Windows(tested), but was tested extensively on Linux (git v2.7.4).

If you encounter any bugs on any platform, please let me know.

## Description ##

This plugin makes it easier to use git in your project by providing commonly used commands. This plugin has easy to access controls without the need of typing commands.

It can also be configured (at minimal) to suit your preference. Plugin configuration is available in user_settings.cfg (located in plugin root folder).

## Requirement ##

1. Git installed correctly on your machine

## Notice

1. Make sure your git email and name has been set correctly
2. Make sure your project directory is a git directory (clone or init)

## Reason for keeping plugin in Alpha ##

1. Bug checking
2. Addition of more features

## What missing ##

1. Push: This was not added due to some known issues, but I may consider adding them in the future if I find a workaround
2. Some commands I may not remember to add

Hope you find it useful.



